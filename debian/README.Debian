tiny-initramfs for Debian
-------------------------

tiny-initramfs is split into two packages in Debian: tiny-initramfs and
tiny-initramfs-core.

The tiny-initramfs-core package contains the statically linked init
binaries, as well as the mktirfs script that creates the initramfs
image that may be used to boot Debian systems. It may be conistalled
with any other initramfs implementation in Debian, and is ideal if one
wants to test it while keeping a bootable image around.

The tiny-initramfs package contains the update-tirfs script to
automatically generate initramfs images for installed kernels, as well
as the required integration hooks to automatically do so when
necessary. It replaces the dracut and initramfs-tools packages.

Note that tiny-initramfs is designed with a limited set of use cases in
mind, and may not be suitable for your specific setup. Please take a
look at README.md to see what features are currently supported.

Testing tiny-initramfs
----------------------

To test tiny-initramfs on your system, install the tiny-initramfs-core
package and run:

 mktirfs -o /boot/initrd.img.tiny-$(uname -r)

Reboot your system and use initrd.img.tiny.KERNELVERSION instead of the
regular initrd.img.KERNELVERSION.

Please read the mktirfs(8) man page for details on how to change how
the initramfs image is put together.

 -- Christian Seiler <christian@iwakd.de>  Wed, 27 Jan 2016 00:01:31 +0100
